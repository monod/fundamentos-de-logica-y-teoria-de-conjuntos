# Fundamentos de Logíca y Teoría de Conjuntos

Reedición del libro Fundamentos de Logíca y Teoría de Conjuntos del Profesor Alberto Jaramillo Atehortua publicado bajo la licencia Creative Commons by-nc-sa

## Propósito
El propósito es reeditarlo en un formato digital más flexible que el actual a la medida que hago la lectura del mismo.

Los formatos candidatos son: markdown, org, restructured y para los diagramas plantuml u otro.

La idea es poder generar formato epub, odt, pdf y otros de manera más fácil.

### Conversion bruta a org usando pandoc

se realiza en la carpeta **original-pandoc** ejecutando los comandos:
```sh
find ./ -name "*.htm*" -exec iconv -t UTF-8//TRANSLIT {} -o {} \;
find ./ -name "*.htm*" -exec iconv -t UTF-8//TRANSLIT {} -o {} \;
find ./ -name "*.htm" -exec rm {} \;
find ./ -name "*.html" -exec rm {} \;
```

## Fuente
En la carpeta **original** se encuentra una replica del contenido realizada con wget así:

```sh
wget -r wget -r http://docencia.udea.edu.co/cen/logica/
mv docencia.udea.edu.co/cen/logica original
```

## Licencia

La licencia es [Creative Commons by-nc-sa](https://creativecommons.org/licenses/by-nc-sa/2.5/co/ "Creative Commons by-nc-sa") puesto que esta es una obra derivada y está obligada a llevar la misma licencia.

El autor original del libro es Profesor Alberto Jaramillo Atehortua, esta edición no intenta realizar cambios de contenido solo de presentación y el material original se puede consultar en [http://docencia.udea.edu.co/cen/logica/](http://docencia.udea.edu.co/cen/logica/ "Portal UdeA").
