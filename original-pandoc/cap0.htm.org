[[file:images/ccap0.gif]]

*0.1-* Introducción<<intro0>>\\
Para una correcta ubicación sobre el estudio que vamos a iniciar,
considero pertinente mostrar algunas teorías acerca de qué es la Lógica
y su relación con la matemática; por esta razón quiero citar lo que el
profesor Alberto Dou, S.J. recopila al respecto.

“El logicismo es una doctrina sobre los fundamentos de la matemática que
considera la lógica como anterior o más fundamental que la matemática y
efectúa la reducción de los conceptos y métodos de inferencia matemática
a los correspondientes de la lógica, concluyendo consiguientemente que
la matemática no es más que una rama de la lógica. Las relaciones que
existan entre la matemática y la lógica pueden constituir un aspecto muy
importante de una teoría sobre fundamentos. Una primera dificultad para
la explicación de estas relaciones es que no sabemos con precisión qué
es la matemática y aún menos qué es la lógica.

Precisamente nos proponemos explicar qué es la matemática, pero entonces
parece que es imprescindible que sepamos previamente qué es la lógica,
si hemos de explicar aquella en términos de ésta.

Ahora bien, una definición precisa de la lógica, que la caracterice
totalmente, depende del sistema filosófico que se adopte. Para nuestro
fin bastará que demos las definiciones siguientes:

- La lógica matemática es una ciencia que es anterior a las demás, que
  contiene las ideas y los principios en que se basan todas las
  ciencias.

Esta definición parcial es de K. Gödel (1906) y me parece que es también
según la mente de Santo Tomás y análogamente de Aristóteles y de la
escolástica medieval. Aunque conviene notar que en Aristóteles y en la
escolástica la lógica es generalmente considerada como un arte que como
una ciencia; un arte que da la manera de operar válidamente con
conceptos y proposiciones, y no como ciencia, porque carece de objetos
propios a los que corresponda algo en la naturaleza.

Una definición muy precisa y moderna es la de A. Church (1903).

- 

  Lógica es el estudio sistemático de la estructura de las proposiciones
  y de las condiciones generales de válida inferencia por un método que
  abstraiga del contenido o materia de las proposiciones y tenga en
  cuenta solamente su forma lógica. Se distingue entre materia y forma
  cuando distinguimos entre la legitimidad lógica o validez de un texto
  razonado y la verdad de las premisas de las cuales se deduce; y en
  este sentido es familiar en el lenguaje ordinario. Sin embargo, es
  necesario establecer con precisión la distinción con referencia a un\\
  lenguaje particular o sistema de notación, un lenguaje formalizado, el
  cual evite las inexactitudes y las irregularidades de estructura y
  expresión que sistemáticamente llevan a equivocaciones y que se
  encuentran en los lenguajes ordinarios, y el cual siga o reproduzca la
  forma lógica, a costa de la brevedad y facilidad de comunicación
  cuando sea necesario. De modo que adoptar un lenguaje formalizado
  particular es adoptar un sistema o teoría particular de análisis
  lógico. Entonces se puede caracterizar el método formal diciendo que
  se\\
  trata de la forma objetiva de las sentencias que expresan
  proposiciones, y suministra en estos concretos términos criterios para
  determinar si las sentencias tienen sentido, criterios de inferencia
  válida, y de otras nociones estrechamente asociadas a éstas.

Por consiguiente, según el logicismo la matemática es una rama de la
lógica, sin duda extensa y con vida propia, pero cuyo método se
identifica con el propio método de la lógica. Se concibe así la
matemática como una disciplina universal que regiría todas las formas de
argumentación.

Una disciplina así fue probablemente pensada por R. Llull (1231-1315) y
por J. Caramuel (1606-1682) y ciertamente por R. Descartes (1596-1650).
El primero en formularla con cierta precisión parece fue G. Leibniz
(1646-1716), quien una vez la definió con estos términos:

Si no hubiese estado reclamado por tantos asuntos, o si hubiese sido más
joven o tenido colaboradores jóvenes en situación de ayudarme, hubiera
confiado dar una especie de álgebra generalizada (spécieuse générale),
en la cual todas las verdades de razón se habrían reducido a un cálculo.
Este sería simultáneamente una especie de lenguaje o escritura
universal, pero infinitamente distinto de todos los propuestos hasta
ahora; y errores, con excepción de los de hecho, podrían darse
únicamente como equivocaciones de cálculo. Sería muy difícil crear o
inventar este cálculo o característica, pero muy fácil aprenderla sin
ayuda de diccionario.

El primero en desarrollarla con considerable extensión y con todo rigor
fue G. Frege (1848-1925), con mayor extensión aún J. Peano (1858-1932)
y, finalmente, A.N. Whitehead (1861-1947) y B. Russell (1872-1970), que
son considerados los introductores del logicismo.

Una de las tareas fundamentales del logicismo es la reducción de los
conceptos matemáticos a conceptos lógicos; a esta reducción se le
designa como logificación”

*0.2* - ¿Por qué es necesaria una buena formación en lógica
?<<formalog0>>

Lo expuesto en el numeral anterior, nos empieza a mostrar la importancia
de esta área del conocimiento y podría llevarnos a considerar la
irrelevancia de la pregunta. No obstante considero que ella cobra plena
vigencia, mucho más por el período histórico al que asistimos y en el
cual quiero ubicarla.

Las respuestas a esta pregunta son muchas y bien fundamentadas por los
especialistas en esta área y también por otras personas, no
necesariamente expertas en ella pero que por formación, la asumen como
una componente inherente a la buena argumentación, al buen juicio. Por
ello quiero más bien considerar otros aspectos que se relacionan con la
argumentación lógica, y en particular con el manejo de la información
producida por diferentes medios.

El avance increíble en los medios de comunicación que caracterizaron las
últimas décadas del siglo veinte y que en el comienzo del nuevo siglo,
hace que cualquier fantasía en este campo sea rápidamente una realidad.
Nos provee de información continua en todas las áreas, sin embargo en
muchos casos la cantidad de ésta no se equipara con su calidad. Es, en
particular, en este aspecto: el procesamiento o análisis crítico,
individual y colectivo de la información recibida y producida, en
diferentes modalidades, sobre el que quiero llamar la atención; y al
respecto destaco con referencia a él, los siguientes elementos.

- La manipulación de la información, manifiesta desde los mensajes
  publicitarios cotidianos inductores al consumismo desmedido, plagados
  de falacias hábilmente diseñadas por sus autores por un lado y
  aceptadas por el desconocimiento del público por el otro.\\
- Las posiciones políticas sostenidas públicamente por líderes
  mundiales, que no resisten el menor análisis por las contradicciones y
  sofismas que conllevan, pero que pasan airosos en todos los auditorios
  sin ninguna crítica a su contenido.

- 

  La ausencia preocupante de interlocutores válidos, veraces, objetivos,
  en diferentes escenarios donde se toman decisiones cruciales que
  pueden afectar desde una población local hasta la humanidad completa,
  nos llevan a presenciar como la ausencia de la fuerza argumentativa es
  reemplazada, lamentablemente en muchas ocasiones, por la fuerza bruta;
  y en muchos otros la palabra comprometida hoy, mañana se olvida.

Estas situaciones reales parecen apuntar a un “nuevo orden” en el cual
son protagonistas la información liviana, acrítica, convincente,
fácilmente asimilable, donde importa mucho la forma pero muy poco el
fondo; y que inevitablemente me lleva a pensar si ese objetivo ideal de
la argumentación válida con todos los elementos que la caracterizan, que
obviamente está en contravía con este diagnóstico actual, ha caducado y
su existencia se reduce exclusivamente al lenguaje de las matemáticas.

Sin embargo creo que nuestra obligación como seres pensantes, como
integrantes de una cadena evolutiva, como formadores, nos impide
claudicar, y hoy como nunca nos exige cultivar denodadamente todos los
elementos que enriquezcan nuestra racionalidad, que estimulen y
movilicen nuestros esquemas cognitivos. Es en este aspecto esencial
donde considero que la lógica, como disciplina formativa, tiene un papel
importante que tenemos que aprovechar como parte de un proceso educativo
integral; que tenga como objetivo hacernos ciudadanos responsables,
críticos sanos y objetivos, tanto en la información que recibimos como
en los juicios que emitimos.

Es el reto al que nos convoca una sociedad que avanza vertiginosamente
en muchos campos, pero que igualmente nos demanda mejor formación para
ser participantes activos en ella.

*0.3* - El objeto de estudio de la lógica<<objet0>>

Con el propósito de motivar el estudio por esta área del conocimiento
considero importante destacar algunos objetivos de la lógica citados por
el Doctor David Kelley en su libro “the Art of Reasoning with Symbolic
logic”, y he adaptado algunas situaciones reales en las que se evidencia
el papel de la lógica y la necesidad de la coherencia y el análisis
crítico en la argumentación.

Cuando nos concentramos en nuestros pensamientos, nuestro objetivo
normalmente es encontrar la respuesta de algo. Tratamos de responder una
pregunta, resolver un problema, probar una tesis, comprender un texto de
una disciplina específica.

En todos estos casos, podríamos decir que estamos tratando de adquirir
conocimientos que no teníamos antes. Y, en muchas ocasiones, podemos
adquirir este conocimiento por la observación directa. Pero también
tenemos que hacer razonamientos, elaborar inferencias y construir
conclusiones conjuntamente a partir de la información obtenida.

Un objeto esencial de la lógica, siempre, ha sido el estudio de la
inferencia y buena parte de nuestro trabajo estará orientado en esta
dirección. Estudiaremos reglas para evaluar las inferencias y
aprenderemos a distinguir las correctas de las incorrectas. Como una
situación motivadora podemos plantearnos el siguiente problema:

Una legislatura aprobó la ley que exige que el cinturón de seguridad, en
los asientos de los autos, sea utilizado. Los ponentes de esta ley
afirman que los usuarios, que utilizan el cinturón, tienen una mejor
oportunidad, estadísticamente, de sobrevivir en accidentes, frente a
aquellas personas que no los usan. Los oponentes, a menudo, señalan
casos particulares en los cuales alguien sobrevivió debido precisamente
a que no utilizó el cinturón.¿Cuál es la mejor clase de evidencia? Están
los oponentes haciendo uso exagerado de las excepciones. Están los
ponentes haciendo uso adecuado de la estadística.

Asumamos para orientar la discusión, que el uso de los cinturones de
seguridad en los asientos, realmente es seguro. ¿Es esto suficiente para
justificar la ley? No, por sí solo. La mayor seguridad de los cinturones
debería justificar la ley, únicamente, si nosotros aceptamos la posición
de que el gobierno debe exigirnos hacer aquello que propenda por nuestra
seguridad.

Muchas personas pueden defender esta posición. Otras dirán que cada
persona es libre para decidir en estos aspectos. Así, encontramos aquí
dos temas de discusión: La seguridad de los cinturones de los asientos y
la norma adecuada del gobierno. ¿Puede fundamentarse, este último, en la
evidencia estadística? Si no lo es así, ¿qué clase de evidencia es
relevante?

Un propósito de la lógica es responder la pregunta que acabamos de
plantear. La lógica sola no podría decirnos si o no al soporte del
mandato establecido por la ley. Ella nos provee de un método a seguir
para la elaboración de decisiones respaldadas. Nos indica cómo dividir y
subdividir y analizar la información disponible para tener la seguridad
de considerar todos los puntos importantes. Nos proporciona principios
para decidir qué tipo de evidencia es apropiada en una situación
particular. Y nos fija principios para determinar qué peso tiene una
pieza dada como evidencia.

El valor de estas destrezas lógicas tiene un ilimitado campo de
aplicación. En la labor normal de los estudiantes en los cursos, que
realizan, frecuentemente, presentan tesis que entran en competencia y
que requieren de una discusión crítica. En los cursos de filosofía un
tema para la discusión puede ser la libertad versus el determinismo; en
la literatura, pueden generarse diferentes interpretaciones de un
personaje en una obra determinada. La discusión de estas ideas, en forma
crítica, significa presentar razones en pro o en contra de ellas. La
comprensión de una teoría científica, demos por caso una teoría sobre el
origen del universo, nos lleva a la necesidad de comprender muchas cosas
acerca de las evidencias para soportarla.

Finalmente, en nuestra vida diaria, tenemos muchas cosas para hacer unas
más importantes que otras, y aquí también necesitamos sopesar las
razones de cada lado y tratar de considerar todos los elementos
importantes.

La lógica puede también ayudarnos a desarrollar mayor agudeza en el
análisis de la argumentación. Muchos hemos participado en discusiones
frustrantes a causa de caer en círculos viciosos. Esto ocurre, a menudo,
cuando “se habla sin escuchar” a los interlocutores, esto es cuando no
estamos hablando en el mismo contexto. Supongamos que algunos argumentan
que es erróneo, por inmoral, considerar la eutanasia, exclusivamente,
como un procedimiento médico dirigido finalmente a terminar con el
dolor, puesto que está de por medio la vida de una persona. Muchos otros
también pueden argumentar que una persona en estado terminal tiene,
legalmente, el derecho a tomar decisiones sobre su vida. Ambos grupos de
personas están tratando el mismo tema de la eutanasia, pero ellos no
están trabajando el mismo aspecto. Los primeros están tratando de
mostrar que la eutanasia es inmoral, mientras que los segundos están
refutando el hecho de que pueda ser ilegal. Si la eutanasia es aceptable
o inaceptable en términos de la moral, y si es legal o ilegal son
conclusiones diferentes. Ellos están relacionados (lo cual permite que
fácilmente se confundan), pero no son idénticos.

Si ambos grupos pueden identificar la diferencia, ellos pueden encontrar
que no están en desacuerdo, después de todo. En esta situación
particular, el problema de ubicar dos contextos diferentes es
relativamente sencillo, porque los dos puntos de vista están claramente
identificados por dos palabras diferentes “inmoral” versus “ilegal”. Un
problema más difícil se presenta cuando dos personas están utilizando la
misma palabra, pero con distintos significados. Para ilustrar esta
situación observemos la siguiente argumentación.

“Una vez analizados los trabajos presentados por los aspirantes al
cargo, el jurado considera que deben relevarse”. Dos personas que
analizan este enunciado pueden expresar conceptos totalmente opuestos
frente a su interpretación así: La primera considera la palabra
relevarse como una acepción de “relevante” es decir “destacable”, en
este sentido su interpretación del texto anterior corresponde a que los
trabajos presentados por los aspirantes tienen tan buena calidad, que
merecen ser destacados. En tanto la segunda persona asume dicha palabra
como acepción de “relevar”, es decir “reemplazar”, en este sentido, su
interpretación del texto corresponde a que los trabajos presentados por
los aspirantes tienen tan baja calidad que exige que los aspirantes sean
reemplazados y, posiblemente que se haga una nueva convocatoria.

Estas situaciones nos llevan a considerar otra área de gran importancia
en la lógica que corresponde a los conceptos y definiciones. Siempre que
razonamos hacemos uso de palabras abstractas; así en la argumentación
sobre la eutanasia se expresaba si esta era o no “ilegal”, como también
si era o no “inmoral”. Las palabras subrayadas son conceptos abstractos
y el debate nos obliga a ver como se definen con toda precisión. Con
frecuencia estamos hablando fuera de contexto cuando usamos la misma
palabra, pero con diferentes significados. En consecuencia, siempre
debemos utilizar el significado de nuestras palabras tan claro y
explícito como nos sea posible. Algunos conceptos, tales como “verdad”,
son extremadamente difíciles de definir, y grandes pensadores han
invertido buen tiempo de su vida en el esfuerzo de lograrlo. La lógica
no nos garantiza éxito, pero si nos provee de un método a seguir, y el
método nos brinda dividendos inmediatos en términos de la claridad y
precisión de nuestros pensamientos.

Así como es necesario precisar el significado de palabras en el contexto
de un análisis, también podemos emplear el camino inverso, tomar
palabras claves y analizar su significado en distintas áreas. Esto nos
permite, además, una integración temática real de diferentes campos. Por
ejemplo, si se toma la palabra dinero puede abordarse su sentido en
áreas como: la religión en la cual se considera como fuente del mal, y
en la economía como medio de cambio. Como un último aspecto bien
importante a considerar desde el ámbito de la lógica, miremos ahora la
objetividad.

Una buena comprensión de la lógica puede ayudarnos a elaborar buenas
argumentaciones y persuadir a otras personas. Pero este no es el único
valor de la lógica. El objetivo fundamental es adquirir conocimiento
para determinar que es verdadero. Ganar en una argumentación no
significa, necesariamente, que nuestra posición sea correcta, así como
perder no significa, necesariamente, que ella sea errónea. Si una
opinión es verdadera o falsa depende de los hechos. El lograr esta
valoración, como objetivo, es lo que el profesor David Kelley denomina
el arte de la objetividad.

En el contexto de la discusión con otras personas, objetividad significa
no solamente presentar las propias ideas lógicamente, sino también
escuchar lo que las otras personas dicen.

La objetividad no exige que seamos neutrales, que no tomemos partido o
que seamos indiferentes ante el tema en discusión. Requiere que tratemos
de mirar la materia en discusión desde el punto de vista de la otra
persona. Así, si nuestro punto de vista fuera correcto, es raro que una
perspectiva singular revele toda la verdad. Se requiere que escuchemos
con imparcialidad la evidencia y los argumentos de la contraparte. En
esta forma si finalmente refutamos las argumentaciones del oponente,
sabremos exactamente por qué, y así tenemos un mejor conocimiento de
nuestra posición. En cierta forma la objetividad es una actitud que
tenemos para seleccionar, y la lógica no puede hacer la selección por
nosotros. Pero la objetividad también involucra una habilidad. Así, con
el mejor propósito del mundo, no podemos ser realmente objetivos, a
menos que conozcamos como seguir y evaluar los argumentos que
escuchamos, como aislar los temas relevantes, como evitar la ambigüedad
y la vaguedad en el lenguaje que utilizamos; esto requiere, en
consecuencia, un buen conocimiento del tema tratado.

Otro aspecto de la objetividad, particularmente importante, es la
comunicación con las personas. A fin de conseguir que nuestras ideas
puedan avanzar positivamente, debemos tener en cuenta el contexto de las
otras personas. Un punto obvio para alguien y que consideraría que no
amerita siquiera su mención en un determinado texto, puede no ser obvio
para los demás y su omisión puede generar la no comprensión de su
argumentación. La objetividad es la habilidad de regresar sobre nuestros
propios juicios, de tal forma que podamos revisarlos en forma crítica, a
través de la mirada de cualquier persona que no comparta nuestro propio
punto de vista, nuestras preferencias, nuestra idiosincrasia.

Todo lo que nosotros podemos razonablemente pedir de nuestra audiencia
es la habilidad de seguir la coherencia lógica. En este aspecto, la
lógica, como lenguaje, es un sistema convenido sin el cual no podríamos
comunicarnos.

Pero la objetividad no es precisamente una virtud social. Va más allá de
la irrazonabilidad de los puntos de vista de los opositores; y más allá
sensitivamente de las necesidades cognitivas de otros. La habilidad de
regresar sobre nuestra cadena de pensamientos y examinarlos críticamente
es una virtud, aún cuando nuestro objetivo no sea comunicarlos. Es una
virtud porque es la única forma de confrontar los resultados de nuestros
razonamientos, de evitar conclusiones precipitadas, de estar en contacto
con los hechos. El resultado de nuestras propias reflexiones no puede
ser mejor que el proceso por el cual llegamos a ellas. “No existe un
libro de la vida con respuestas en su interior que nos permita ver si
estamos o no en lo correcto. El buen razonamiento es un proceso de
autodirección, autocorrección alerta a los peligros de los juicios
apresurados y nunca temeroso a preguntar o a responder “¿Por qué?”.

Reafirmando algo ya dicho, esto es más bien cuestión de actitud, de
elección, una posición en nuestra vida y la lógica no puede hacer la
selección por nosotros. Pero si nos provee de herramientas útiles. Nos
suministra la br&uacut
