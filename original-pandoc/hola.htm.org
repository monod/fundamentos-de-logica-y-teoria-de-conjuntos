En el absoluto donde existen los que ya partieron,\\
y mientras avanzamos a su encuentro,\\
en el único lenguaje en el que con ellos nos comunicamos:\\
el silencio......

Surgen los interrogantes constantes, eternos:\\
¿Cual es el espacio real?\\
¿Que es lo cierto?\\
¿Quien es el que vive?\\
¿Quien está muerto?

En memoria a\\
Bernardo Jaramillo A.

[[http://creativecommons.org/licenses/by-nc-sa/2.5/co/][\\
[[file:imagenes%20logica/BY_NC_SA.png]]]]\\
Esta obra es publicada bajo una\\
[[http://creativecommons.org/licenses/by-nc-sa/2.5/co/][licencia
Creative Commons]]

ISBN 958-655-924-6

[[file:images/separador.gif]]

Visitante Nro.\\
[[http://cci.udea.edu.co/contador/registrar.php?usuario=logica]]
